<?php include "views/headerview.php";?>
<body>
<div class="trombi-grid">

<?php

foreach($jsonDecode as $learner) : ?>
    <div class="cards">
        <div class="name">
            <p><?php echo($learner->nom); ?></p>
            <p><?php echo($learner->prenom); ?></p>
        </div>
        <img class="pics" src="<?php echo($learner->image) ?>">
        
        
        <div class="promotions">
        <p><?php echo($learner->promotion->name); ?></p>
            
        </div>

        <div class="descriptions">
        <p><?php echo($learner->excerpt->rendered); ?></p>
        </div>
        <div class="competences">
            <p><pre><ul><?php //var_dump($learner->competences); 
                foreach($learner->competences as $competence){
                    echo "<li>".$competence->name."</li>";
                
                }
            ?></ul></pre></p>
        </div>
        <div class="link">
            <a style="text-decoration:none" href="<?php echo($learner->portfolio); ?>">Portfolio</a>
            <a style="text-decoration:none" href="<?php echo($learner->cv); ?>"> Curriculum </a>
            <a style="text-decoration:none" href="<?php echo($learner->linkedin); ?>"> Linkedin </a>
        </div>
    </div>
 
<?php endforeach; ?>
    </div>
</body>
<?php include "views/footerview.php";?>
