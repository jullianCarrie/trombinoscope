<?php

class viewTemplate //creation view template class
{
    /**
     * @var string
     */
    private $templateTdr;

    public function __construct($templateTdr) //the constructor call the next methode 
    {
        $this->templateDir = $templateTdr;
    }

    /**
     * {@inheritDoc}
     */
    public function render($template, array $parameters = array()) //creation of bord parameter 
    {
        extract($parameters); //variable import

        if (false === $this->isAbsoluteWay($template)) { // the path do not stay in the same place
            $template = $this->templateTdr . DIRECTORY_SEPARATOR . $template; // path adaptable for every plateform
        }

        ob_start();  //save the datas in the cach
        include $template;

        return ob_get_clean(); // return the cach, that permit to clear all the informations under it
    }

    private function isAbsoluteWay($file)
    {
        if (strspn($file, '/\\', 0, 1) // define the larger of the segment between 0 and one
            || (strlen($file) > 3 && ctype_alpha($file[0]) // caculate size file higer that 3 and verificate the firste element on the file
            && substr($file, 1, 1) === ':' //return the position file in the first place
            && (strspn($file, '/\\', 2, 1)) //define the larger of the segment between
        )
        || null !== parse_url($file, PHP_URL_SCHEME)// Url segmentation
        ) {
            return true;
        }

        return false;
    }
}
